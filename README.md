[![Build status](https://gitlab.com/timelord/sklearn_transformers/badges/master/build.svg)](https://gitlab.com/timelord/sklearn_transformers/commits/master)

# Transformers
A library to use in conjunction with the libraries [scikit-learn](http://scikit-learn.org/), [sklearn_pandas](https://github.com/scikit-learn-contrib/sklearn-pandas) and [Dask](https://dask.pydata.org/en/latest/) to preprocess your data.

### Idea
This library is a codebase consisting exclusively out of `scikit-learn` like-transformers. It utilizes the `fit`, `transform`, `inverse_transform` methods and raises `NotFittedError` exceptions when the transformer is not fitted. 

### Design
Each and every transformer inherits the following classes:
`TransformerMixin`, `BaseEstimator`.
Whenever paralliziation comes into the mix, the goal is to use the implementation of [Joblib](https://pythonhosted.org/joblib/index.html) to allow it to be used in the [Dask](https://dask.pydata.org/en/latest/) framework as well.

An example idealized transformer will look like this. It's almost as simple as possible, yet provides the necessary complexity to be a comprehensive example of a desired transformer. Though often the methods `fit` and `inverse_transform` aren't necessary for a transformation. Though for most transformers this is not a requirement.


```python
import numpy as np
from sklearn.base import TransformerMixin, BaseEstimator
from sklearn.exceptions import NotFittedError

class SimpleTransformer(TransformerMixin, BaseEstimator):
    name = "SimpleTransformer" #Give the transformer the name it has.
    
    def fit(self, X, y=None): #Fit something
        self.max_val = max(X)
        return self
    
    def transform(self, X): #Transform something
        if hasattr(self, "max_val"):
            X += self.max_val
        else: #Raise the appropriate error when not fitted
            raise NotFittedError(f"{self.name} has not been fitted yet.")
        return X
    
    def inverse_transform(self, X): #Implement an inverse_transform
        if hasattr(self, "max_val"):
            X -= self.max_val
        else: #Raise the appropriate error when not fitted
            raise NotFittedError(f"{self.name} has not been fitted yet.")
        return X    
    
s = SimpleTransformer()
s.fit_transform(np.array([2,5]))
```

### Goal
The goal is to show people how beautiful the API of `scikit-learn` is and how they can write transformers and contribute to this repository.
People hopefully will embrace the `scikit-learn` way of preprocessing data and write generalizable transformers instead of custom-made scripts that are hard to re-use.

### Where does this package stand
The original `scikit-learn` transformer useage is on Arrays. Luckily pandas DataFrame use NumPy Arrays under the hood and thus they also work (most often) within the transformers. A library called [sklearn_pandas](https://github.com/scikit-learn-contrib/sklearn-pandas) is already in existence and bridges the difference between Arrays and DataFrames. This package aims to contribute useable transformers in addition to that bridge by delivering a large set of unique transformers.

### Array size immutability
Normally the `scikit-learn` transformers do not change the size or order of the inputted arrays, this provides highly predictable useage. This library does _not_ aim to provide an immmutable input-output array size experience. Instead it implements another type of transformer: A transformer that _does_ mutate the array shape.


### Tests
Tests are run by pytest and calls the added transformers in several pipeline tests. The goal is to check for simple errors but also less trivial errors, like not correctly implementing the `fit` or `transform` methods. 

The tests can be viewed [here](https://gitlab.com/timelord/sklearn_transformers/blob/master/tests/test_pipeline.py)