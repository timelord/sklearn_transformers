import numpy as np
from sklearn.exceptions import NotFittedError
from sklearn.base import BaseEstimator, TransformerMixin

class ExpandedTransformerMixin(TransformerMixin, BaseEstimator):
    
    def fit(self, X, y=None):
        return self
    
    def transform(self, X):
        return X
    
    def inverse_transform(self, X):
        return X


class SimpleTransformer(ExpandedTransformerMixin):
    name = "SimpleTransformer" #Give the transformer the name it has.
    
    def fit(self, X, y=None): #Fit something
        self.max_val = np.max(X)
        return self
    
    def transform(self, X): #Transform something
        if hasattr(self, "max_val"):
            X += self.max_val
        else: #Raise the appropriate error when not fitted
            raise NotFittedError(f"{self.name} has not been fitted yet.")
        return X
    
    def inverse_transform(self, X): #Implement an inverse_transform
        if hasattr(self, "max_val"):
            X -= self.max_val
        else: #Raise the appropriate error when not fitted
            raise NotFittedError(f"{self.name} has not been fitted yet.")
        return X    


class CompareDatesTransformer(ExpandedTransformerMixin):
    
    def __init__(self, lag_feature_dict):
        #Maybe allow selection of specific columns in the dict?
        self.lag_feature_dict = lag_feature_dict
    
    def _make_comparison(self, X, max_val, low_val):
        if X[max_val] < X[low_val]:
            return 2 #Longer
        elif X[max_val] == X[low_val]:
            return 1 #Same
        elif X[max_val] > X[low_val]:
            return 0 #Shorter
        else:
            return -1 #NaN
        
    def fit(self, X, y=None):
        self.compare_tuples = []
        for columns in self.lag_feature_dict.values():
            max_col = columns[0]
            for col in columns[1:]:
                self.compare_tuples.append([max_col, col])
        return self
    
    def transform(self, X):
        for compare_tuple in self.compare_tuples:
            max_val = compare_tuple[0]
            low_val = compare_tuple[1]
            col_name = max_val + '_' + low_val
            X[col_name] = X.apply(lambda x: self._make_comparison(x, max_val, low_val), axis =1)
        return X
  

class GroupDummiesAgg(ExpandedTransformerMixin):
    """ 
    Used for dataframes with multiple rows per identical product ID to make group aggregates per ID column.
    Can be used as a static & temporal feature for training.


    Parameters
    ----------
    df : pandas DataFrame

    Returns
    ----------
    df : pandas DataFrame
    """       

    def __init__(self, id_column, target_id_column, target_column, groupby_ops = 'sum'):
        self.id_column = id_column
        self.target_id_column = target_id_column
        self.target_column = target_column
        self.groupby_ops = groupby_ops
        
    def fit(self, X, y=None):
        return self
    
    def transform(self, X):
        ndf = X[[self.id_column, 
                 self.target_id_column, 
                 self.target_column]].drop_duplicates() #Product groep per afgenomen product.
        ndf = pd.concat([ndf[[self.id_column]], 
                         pd.get_dummies(ndf[column])], 
                        axis =1)
        grouped = ndf.groupby(self.id_column)
        X = getattr(grouped, self.groupby_ops)()
        return X

class MakeDummies(ExpandedTransformerMixin):
    """ 
    TODO

    Parameters
    ----------
    df : pandas DataFrame

    Returns
    ----------
    df : pandas DataFrame
    """       

    def __init__(self, id_column, target_column):
        self.id_column = id_column
        self.target_column = target_column
        
    def fit(self, X, y=None):
        return self
    
    def transform(self, X):
        X = X[[self.id_column, 
                 self.target_column]].drop_duplicates() #Product groep per afgenomen product.
        X = pd.get_dummies(X)
        X = X.set_index(self.id_column)
        return X  

class ApplyMapColumn(ExpandedTransformerMixin):
    """ 
    TODO

    Parameters
    ----------
    df : pandas DataFrame

    Returns
    ----------
    df : pandas DataFrame
    """       

    def __init__(self, target_column, map_dict):
        self.target_column = target_column
        self.map_dict = map_dict
        
    def fit(self, X, y=None):
        return self
    
    def transform(self, X):
        X[self.target_column]= X[self.target_column].map(self.map_dict)
        return X
    
    
class ConvertBoolean(ExpandedTransformerMixin):
    
    def __init__(self, target_column, map_dict = {True: 1, False: 0, None: -1}):
        self.target_column = target_column
        self.map_dict = map_dict
        
    def fit(self, X, y=None):
        return self
    
    def transform(self, X):
        X[self.target_column] = X[self.target_column].map(self.map_dict)
        X[self.target_column] = X[self.target_column].fillna(self.map_dict[None])
        return X
    
class SplitColumn(ExpandedTransformerMixin):
    """ 
    Splits a column of a dataframe in seperate columns based on the internal values.
    Each value that is provided in the split_on_values argument will be used to create a new column
    containing the values in the value_column exclusively.

    #TODO: Keep track of column name changes?
    Parameters
    ----------
    df : pandas DataFrame

    Returns
    ----------
    df : pandas DataFrame
    """    
    def __init__(self, split_column, split_on_values, value_column):
        self.split_column = split_column
        self.split_on_values = split_on_values
        self.value_column = value_column
        
        
    def _create_column_name(self, val):
        return val + '_' + self.value_column
    
    def fit(self, X, y=None):
        self.val_dict = {}
        for val in self.split_on_values:
            self.val_dict[val] = X.loc[X[self.split_column] == val][self.value_column]
        return self
    
    def transform(self, X):
        for k, v in self.val_dict.items():
            column_name = self._create_column_name(k)
            X[column_name] = v
        return X
