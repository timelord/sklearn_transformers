# -*- coding: utf-8 -*-
# Part of Transformers+. See LICENSE file for full copyright and licensing details.

"""Project metadata

Information describing the project.
"""

# The package name, which is also the 'UNIX name' for the project.
package = 'sklearn_transformers'
project = 'A framework to assimilate transformer objects from all kinds of flavors.'
description = 'A framework to assimilate transformer objects from all kinds of flavors.'

version = "2018.05.11"

authors = [ 'Casper Lubbers',
            'John Ciocoiu']

authors_string = ', '.join(authors)
emails = [ 'john.ciocoiu@kpn.com',
           'casper.lubbers@kpn.com']
license = 'GPL-3.0'
copyright = '2018 ' + authors_string
url = 'https://gitlab.com/timelord/Transformers'
