import numpy as np
from sklearn.base import TransformerMixin, BaseEstimator
from sklearn.exceptions import NotFittedError

class SimpleTransformer(TransformerMixin, BaseEstimator):
    name = "SimpleTransformer" #Give the transformer the name it has.
    
    def fit(self, X, y=None): #Fit something
        self.max_val = np.max(X)
        return self
    
    def transform(self, X): #Transform something
        if hasattr(self, "max_val"):
            X += self.max_val
        else: #Raise the appropriate error when not fitted
            raise NotFittedError(f"{self.name} has not been fitted yet.")
        return X
    
    def inverse_transform(self, X): #Implement an inverse_transform
        if hasattr(self, "max_val"):
            X -= self.max_val
        else: #Raise the appropriate error when not fitted
            raise NotFittedError(f"{self.name} has not been fitted yet.")
        return X    
    
s = SimpleTransformer()
s.fit_transform(np.array([2,5]))