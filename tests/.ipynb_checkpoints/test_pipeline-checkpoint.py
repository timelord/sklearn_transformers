from transformers.example_transformer import SimpleTransformer 
import numpy as np
import pandas as pd
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.cluster import KMeans

x,y = 10, 3
test_ar = np.random.random((x, y))
test_df = pd.DataFrame(test_ar, 
                      columns = [chr(65+i) for i in range(y)])

def test_IsTransformer():
    #Check if it has the right attrs
    s = SimpleTransformer()
    assert hasattr(s, 'fit')
    assert hasattr(s, 'transform')
    assert hasattr(s, 'inverse_transform')
    assert hasattr(s, 'fit_transform')

def test_AltersArrayShape():
    #Check if it influence the shape, not necesarrily a bad thing if it does so.
    s = SimpleTransformer()
    ar = s.fit_transform(test_ar)
    assert ar.shape == test_ar.shape
    
    
def test_AltersArrayLength():
    #The length / width tests are there to indicate what kind of transformer it is.
    #This test is a subset of the ArrayShape
    s = SimpleTransformer()
    ar = s.fit_transform(test_ar)
    assert ar.shape[0] == test_ar.shape[0]
   

def test_AltersArrayWidth():
    #The length / width tests are there to indicate what kind of transformer it is.
    #This test is a subset of the ArrayShape
    s = SimpleTransformer()
    ar = s.fit_transform(test_ar)
    assert ar.shape[1] == test_ar.shape[1]
    
    
    
def test_RunsInPipeline():
    #Have passed test_IsTransformer, it should also be able to pass test.
    #Always good to check it in a real-life like pipeline.
    s = SimpleTransformer()
    km = KMeans()
    pipe = Pipeline(steps =
                   [('simple', s),
                    ('kmean', km)])
    res = pipe.fit_predict(test_ar)
    print(res)
    
    
def test_RunsInGridSearchCV():
    #Have passed test_IsTransformer, it should also be able to pass test.
    #Always good to check it in a real-life like pipeline.
    s = SimpleTransformer()
    km = KMeans()
    params = {'kmean__n_clusters': [3, 6]}
    pipe = Pipeline(steps =
                   [('simple', s),
                    ('kmean', km)])
    grid = GridSearchCV(pipe, params)
    res = grid.fit(test_ar).predict(test_ar)
    print(res)
    
    
